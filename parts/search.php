<div class="c-search">

    <div class="c-search__bg"></div>

    <div class="c-search__bar">
        <div class="container flex justify-between items-center">
            <form action="<?php echo site_url(); ?>" method="get">
                <input type="search" name="s" placeholder="Search...">
            </form>
            <a class="jq-search" href="#">
                ✕
            </a>
        </div>
    </div>

</div>
