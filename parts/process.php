<div class="bg-turquoise py-16">

	<div class="owl-carousel">

		<div class="owl-carousel__slide--first text-white">

			<?php the_field('left_content', 2); ?>

		</div>

		<?php if ( have_rows('panels', 2) ) : ?>

			<?php while ( have_rows('panels', 2) ) : the_row(); ?>

				<div class="owl-carousel__slide">

					<h1><?php the_sub_field('number'); ?></h1>

					<p><?php the_sub_field('title'); ?></p>

					<p><?php the_sub_field('text'); ?></p>

				</div>

			<?php endwhile; ?>

		<?php endif; ?>

	</div>

</div>
