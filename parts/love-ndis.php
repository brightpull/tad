<div class="container py-16 flex items-center justify-between hide-print">

	<img width="100" class="mr-4 md:mr-12" src="<?php echo get_template_directory_uri(); ?>/images/ndis.png" alt="NDIS logo">

	<p class="text-2xl md:text-3xl leading-tight mb-0"><?php the_field('white_intro', 2); ?></p>

</div>
