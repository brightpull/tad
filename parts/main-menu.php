<header class="bg-white py-10 hide-print">

    <div class="container flex items-center justify-between">

        <a href="<?php echo get_permalink( 2 ); ?>">
            <img width="200" src="<?php echo get_template_directory_uri(); ?>/svg/logo-black.svg" alt="TAD Australia logo">
        </a>

        <ul class="c-menu-desktop">
            <?php wp_list_pages( 'depth=2&title_li=&exclude=2,18,117' ); ?>
        </ul>

        <a class="jq-menu-mobile text-xl md:hidden" href="#">
            <i class="fas fa-bars"></i>
        </a>

    </div>

</header>
