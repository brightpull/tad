<div class="bg-turquoise py-4 text-white hide-print">

    <div class="container flex justify-between">

        <p class="mb-0">Call us on <span class="font-bold">1300 663 243</span></p>

        <ul class="flex items-center mb-0">
            <li class="hidden mr-2 md:block">
                <a class="u-button--sm text-xs jq-dyslexic" href="#" style="background: rgba(0, 0, 0, 0.25);">
                    Dyslexic Font
                </a>
            </li>
            <li class="hidden md:block mr-2">
                <a class="u-button--sm text-xs" href="https://talkify.net/text-to-speech" target="_blank" style="background: rgba(0, 0, 0, 0.75);">
                    Talkify
                </a>
            </li>
            <!-- <li class="hidden md:block mr-6">
                <a class="u-button--sm text-xs" href="<?php echo get_permalink( 18 ); ?>" style="background: rgba(0, 0, 0, 0.75);">
                    Contact Us
                </a>
            </li> -->
            <li>
                <a class="jq-search" href="#">
                    <i class="fas fa-search"></i>
                </a>
            </li>
        </ul>

    </div>

</div>
