<?php get_header(); ?>

<div class="py-32 text-center text-white hide-print" style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('<?php echo get_the_post_thumbnail_url(); ?>'); background-position: center center; background-size: cover;">

	<div class="container">

		<h1 class="mb-0 md:leading-tight md:text-5xl">404</h1>

	</div>

</div>

<div class="bg-white">

	<div class="container grid-sidebar pt-16 pb-0 md:pb-16">

		<div class="content-area">

			<h2>Page not found</h2>

	        <p>Please check the URL and try again.</p>

		</div>

	</div>

</div>

<?php get_footer(); ?>
