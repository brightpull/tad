<div class="c-sidebar hide-print">

	<div class="bg-turquoise mb-8 pt-10 pb-4 px-6 text-center">

		<div class="text-white">

			<h4>Get in touch with your local AT provider</h4>

			<p>Simply fill out the following form or select your state below</p>

		</div>

		<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]'); ?>

	</div>

	<?php if ( ! get_field('hide_state_boxes')) : ?>

		<ul class="c-states">

			<li>
				<a href="<?php the_field( 'act', 2 ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/act.png" alt="ACT">
					ACT
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'nsw', 2 ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nsw.jpg" alt="NSW">
					New South Wales
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'qld', 2 ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/qld.png" alt="QLD">
					Queensland
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'sa', 2 ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/sa.png" alt="SA">
					South Australia
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'tas', 2 ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/tas.png" alt="TAS">
					Tasmania
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'vic', 2 ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/vic.png" alt="VIC">
					Victoria
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'wa', 2 ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/wa.png" alt="WA">
					Western Australia
				</a>
			</li>

		</ul>

	<?php endif; ?>

</div>
