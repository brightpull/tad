// Animate on scroll
// AOS.init();

// MAIN MENU
jQuery('.c-menu-desktop .page_item_has_children').mouseover(function() {
	jQuery(this).children('a').siblings('ul.children').show();
});
jQuery('.c-menu-desktop .page_item_has_children').mouseout(function() {
	jQuery(this).children('a').siblings('ul.children').hide();
});

// HERO
jQuery(document).ready(function(){
	jQuery('.c-hero').slick({
		fade: true,
		autoplay: true,
		autoplaySpeed: 2500,
		prevArrow: '<span class="slick-arrow slick-prev">‹</span>',
		nextArrow: '<span class="slick-arrow slick-next">›</span>'
	});
});

// ASSISTIVE CAROUSEL
jQuery(document).ready(function(){
	jQuery('.owl-carousel').owlCarousel({
		dots: false,
		nav: true,
		responsive : {
			0: {
				items: 1,
				margin: 0,
				autoWidth: false,
			},
			768: {
				items: 4,
				margin: 16,
				autoWidth: true,
			},
		}
	});
});

// FEEDBACK FORM
jQuery('.jq-feedback').click(function(e) {
	e.preventDefault();
	jQuery('.c-feedback__wrap').fadeToggle();
});

// YOUTUBE VIDEO
jQuery('.jq-youtube').click(function(e) {
	e.preventDefault();
	jQuery('.c-youtube__wrap').fadeToggle();
});

// DYSLEXIC FONT
jQuery(document).ready(function() {
	if (Cookies.get('dyslexic') == 1) {
		jQuery('body').css('font-family', 'Dyslexie');
		jQuery('.jq-dyslexic').html('Standard Font');
	} else {
		jQuery('body').css('font-family', 'Montserrat');
		jQuery('.jq-dyslexic').html('Dyslexic Font');
	}
});
jQuery('.jq-dyslexic').click(function(e) {
	e.preventDefault();
	if (Cookies.get('dyslexic') == 1) {
		Cookies.set('dyslexic', 0);
		jQuery('body').css('font-family', 'Montserrat');
		jQuery(this).html('Dyslexic Font');
	} else {
		Cookies.set('dyslexic', 1);
		jQuery('body').css('font-family', 'Dyslexie');
		jQuery(this).html('Standard Font');
	}
});

// SEARCH FORM
jQuery('.jq-search').click(function(e) {
	e.preventDefault();
	jQuery('.c-search__bar').slideToggle();
    jQuery('.c-search__bg').fadeToggle();
    jQuery('.c-search input').focus();
});

// MOBILE MENU
jQuery('.jq-menu-mobile').click(function(e) {
	e.preventDefault();
	jQuery('.c-menu-mobile').slideToggle();
});
