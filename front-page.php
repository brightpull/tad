<?php get_header(); ?>

<div class="c-hero">
	<?php if ( have_rows( 'slides' ) ) : ?>
	    <?php while ( have_rows( 'slides' ) ) : the_row(); ?>
		<div class="bg-center bg-cover" style="background-image:url(<?php the_sub_field( 'background' ); ?>);">
			<div class="container">
				<div class="md:px-6">
					<?php the_sub_field( 'title' ); ?>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>

<div class="bg-white pt-16">

	<div class="container relative">

		<div class="tuck-dark-box bg-charcoal p-8 justify-between items-center md:p-12 md:flex">

			<p class="text-2xl leading-tight text-white md:mb-0 md:text-3xl"><?php the_field('charcoal_banner', 2); ?></p>

			<div class="md:ml-12">
				<a class="u-button--lg bg-turquoise inline-block w-64" href="#anchor-states">
					Select your State
				</a>
			</div>

		</div>

	</div>

	<div class="md:mt-10">
		<?php echo get_template_part( 'parts/love-ndis' ); ?>
	</div>

</div>

<?php echo get_template_part( 'parts/process' ); ?>

<div class="py-16">

	<div class="container grid-2">

		<div>

			<iframe class="mb-8 w-full" width="560" height="315" src="https://www.youtube.com/embed/<?php the_field('left_video_id', 2); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

			<?php the_field('left_video_content', 2); ?>

		</div>

		<div>

			<iframe class="mb-8 w-full" width="560" height="315" src="https://www.youtube.com/embed/<?php the_field('right_video_id', 2); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

			<?php the_field('right_video_content', 2); ?>

		</div>

	</div>

</div>

<div class="bg-turquoise md:text-center py-16 md:pb-32">

	<div class="container md:max-w-3xl">

		<div class="text-white">

			<h2 class="text-5xl leading-tight">Get in touch with your local AT provider</h2>

			<p class="text-2xl leading-tight">TAD Australia members are registered NDIS providers with services available to people of all ages and abilities.</p>

		</div>

		<div class="c-form-home">
			<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]'); ?>
		</div>

		<p class="text-white text-2xl leading-tight mb-0">We have solutions for your independence, wherever you are!</p>

	</div>

</div>

<div class="bg-white py-16">

	<div class="container relative">

		<ul class="grid-states" id="anchor-states">

			<li>
				<a href="<?php the_field( 'act' ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/act.png" alt="ACT">
					ACT
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'nsw' ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/nsw.jpg" alt="NSW">
					New South Wales
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'qld' ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/qld.png" alt="QLD">
					Queensland
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'sa' ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/sa.png" alt="SA">
					South Australia
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'tas' ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/tas.png" alt="TAS">
					Tasmania
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'vic' ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/vic.png" alt="VIC">
					Victoria
				</a>
			</li>
			<li>
				<a href="<?php the_field( 'wa' ); ?>" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/images/wa.png" alt="WA">
					Western Australia
				</a>
			</li>

		</ul>

	</div>

</div>

<div class="c-video">
	<video autoplay loop muted poster="<?php echo get_template_directory_uri(); ?>/images/video.jpg">
		<source src="<?php echo get_template_directory_uri(); ?>/video/play.webm" type="video/webm">
		<source src="<?php echo get_template_directory_uri(); ?>/video/play.mp4" type="video/mp4">
		<source src="<?php echo get_template_directory_uri(); ?>/video/play.ogv" type="video/ogg">
	</video>
	<div style="background-color: rgba(0, 0, 0, 0.3);">
		<div class="container py-16 text-center text-white md:py-32">
			<h2 class="text-5xl mb-16">Providers working together</h2>
			<div class="svg-16 mb-4">
				<a class="jq-youtube" href="#" target="_blank">
					<?php echo get_template_part( 'svg/play' ); ?>
				</a>
			</div>
			<p class="mb-0">
				<a class="jq-youtube" href="#" target="_blank">
					Watch Video
				</a>
			</p>
		</div>
	</div>
</div>

<?php get_footer(); ?>
