<?php get_header(); ?>

<div class="py-32 text-center text-white hide-print" style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('<?php echo get_the_post_thumbnail_url(); ?>'); background-position: center center; background-size: cover;">

	<div class="container">

		<h1 class="mb-0 md:leading-tight md:text-5xl"><?php the_title(); ?></h1>

	</div>

</div>

<div class="bg-gray-200 py-4 text-gray-700 text-sm hide-print">

	<div class="container flex items-center justify-between">

		<p class="mb-0">
			<a href="<?php echo site_url(); ?>">Home</a> >
			<?php if ($post->post_parent) : ?>
				<a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo get_the_title($post->post_parent); ?></a> >
			<?php endif; ?>
			<?php the_title(); ?>
		</p>

		<ul class="flex items-center mb-0">
			<li class="mr-6">
				<a href="javascript:window.print();">
					<i class="fas fa-print mr-2"></i>Print this page
				</a>
			</li>
			<li>
				<a href="?print=pdf">
					<i class="fas fa-file-pdf mr-2"></i>PDF
				</a>
			</li>
		</ul>

	</div>

</div>

<div class="bg-white">

	<div class="container grid-sidebar pt-16 pb-0 md:pb-16">

		<div class="content-area">

			<?php if ( have_posts() ) : ?>

			    <?php while ( have_posts() ) : the_post(); ?>

					<h2 class="hidden show-print"><?php the_title(); ?></h2>

			        <?php the_content(); ?>

			    <?php endwhile; ?>

			<?php endif; ?>

		</div>

		<?php get_sidebar(); ?>

	</div>

</div>

<?php if (is_page(4)) : ?>
	<?php echo get_template_part( 'parts/process' ); ?>
<?php endif; ?>

<?php echo get_template_part( 'parts/love-ndis' ); ?>

<?php get_footer(); ?>
