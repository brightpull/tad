<div class="bg-black text-white py-12 hide-print">

	<div class="container md:flex justify-between">

		<a class="mb-8 md:mb-0 block" href="<?php echo get_permalink( 2 ); ?>">
            <img width="200" src="<?php echo get_template_directory_uri(); ?>/svg/logo-white.svg" alt="TAD Australia logo">
        </a>

		<div class="md:text-right">

			<p class="mb-1 text-sm">Call <span class="font-bold">1300 663 243</span> to be connected in your state</p>

			<p class="mb-0 text-xs">Website &copy; TAD Australia | website by <a href="http://brightagency.com.au" class="hover:text-turquoise" target="_blank">bright</a></p>

		</div>

	</div>

</div>

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148611263-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-148611263-3');
</script>

</body>

</html>
