<?php

// Display SVG
function echo_svg( $name )
{
    echo get_template_part( 'svg/' . $name );
}
