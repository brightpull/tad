<?php get_header(); ?>

<div class="bg-gray-600 py-24 text-white text-center bg-center bg-cover" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">

	<div class="container">

		<h1 class="mb-0 md:text-5xl md:leading-tight">Search results</h1>

	</div>

</div>

<div class="bg-gray-200 py-4 text-sm text-gray-700">

	<div class="container flex items-center justify-between">

		<p class="mb-0"><a href="<?php echo site_url(); ?>">Home</a> > Search results</p>

		<ul class="flex items-center mb-0">
			<li class="mr-6"><a href="#"><i class="fas fa-print mr-2"></i>Print this page</a></li>
			<li><a href="#"><i class="fas fa-file-pdf mr-2"></i>PDF</a></li>
		</ul>

	</div>

</div>

<div class="bg-white">

	<div class="container pt-16 pb-0 md:pb-16 grid-sidebar">

		<div>

            <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <div class="mb-5" style="border-bottom: 1px solid #d7d7d7;">

                        <h4 class="mb-3 text-orange"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>

                        <?php the_excerpt(); ?>

                    </div>

                <?php endwhile; ?>

			<?php else : ?>

				<p>No search results. Please try again.</p>

            <?php endif; ?>

		</div>

		<?php get_sidebar(); ?>

	</div>

</div>

<?php echo get_template_part( 'parts/love-ndis' ); ?>

<?php get_footer(); ?>
