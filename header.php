<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">

    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <!-- Font Awesome -->
    <script src="https://kit.fontawesome.com/8fd5aaf580.js"></script>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php echo get_template_part( 'parts/youtube-video' ); ?>

<?php echo get_template_part( 'parts/feedback' ); ?>

<?php echo get_template_part( 'parts/search' ); ?>

<?php echo get_template_part( 'parts/toolbar' ); ?>

<?php echo get_template_part( 'parts/main-menu' ); ?>

<?php echo get_template_part( 'parts/mobile-menu' ); ?>
