<?php

get_template_part( 'includes/cleanup' );
get_template_part( 'includes/customisations' );
get_template_part( 'includes/helpers' );

// Add theme support
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );

// Enqueue scripts
function bedrock_load_assets()
{
    // jQuery
    if ( ! is_admin() ) { wp_deregister_script( 'jquery' ); }
    wp_enqueue_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), '3.3.1', false );

    // CSS
    // wp_enqueue_style( 'aos', 'https://unpkg.com/aos@next/dist/aos.css' );
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/scss/vendor/slick.css' );
    wp_enqueue_style( 'owl', get_template_directory_uri() . '/scss/vendor/owl.carousel.min.css' );
    wp_enqueue_style( 'owl-theme', get_template_directory_uri() . '/scss/vendor/owl.theme.default.min.css' );
    wp_enqueue_style( 'app', get_template_directory_uri() . '/style.css' );

    // JavaScript
    // wp_enqueue_script( 'aos', 'https://unpkg.com/aos@next/dist/aos.js', array(), '3.0.0', true );
    wp_enqueue_script( 'js-cookie', 'https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'owl', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '2.3.4', true );
    wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array(), '1.8.1', true );
    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app-min.js', array(), null, true );
}
add_action( 'wp_enqueue_scripts', 'bedrock_load_assets' );

// TinyMCE custom styles
// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {
	// Define the style_formats array
	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'Button',
			'block' => 'a',
			'classes' => 'u-button--sm bg-turquoise inline-block font-normal',
			'wrapper' => true,
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = wp_json_encode( $style_formats );

	return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );
